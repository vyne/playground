## Constraints & Problem solving
Services can describe both constraints they have on inputs, and effects they have on outputs.

Vyne uses this to understand when data fails constraints, and attempts to find resolutions through the available operations.

As an example, let's find out what we can purchase with our available balance.  However, the service that exposes this information
needs to know the balance in GBP, not in points:

```
service RewardsShopService {
   operation getAvailableRewards( RewardsAccountBalance( currencyUnit = "GBP" ) ) : AvailableRewards
}
```

Elsewhere, there's a service that converts rewards in points to GBP, based on today's exchange rate:

```
service RewardsBalanceService {
   @HttpOperation(method = "POST" , url = "/balances/{demo.CurrencyUnit}")
   operation convert(  
        CurrencyUnit, 
        @RequestBody RewardsAccountBalance 
      ) : RewardsAccountBalance( from source, currencyUnit = targetCurrency )
}
```

Vyne can connect these services, to perform the conversion.  Let's take a look.

[[demo]]


