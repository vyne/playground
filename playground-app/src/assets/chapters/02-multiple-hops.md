## Orchestrating further
Now, lets add one more concept - the balance of our rewards cards.

Account balances are stored in a different service from our Marketing service, meaning there's now 3 distinct services in play.

Let's grab the balance for Jim's rewards card:

[[demo]]

So, still just two lines of code, this time orchestrating 3 distinct calls.

 * Vyne understood that in order to get the balance, we must first find the card number
 * Once Vyne had retrieved the card number, it was able to fetch the balance and return.

