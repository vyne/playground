# Introduction

Welcome to the interactive Vyne guide.

Vyne automates integration between services, by leveraging type data and metadata on services.

This guide demonstrates some of the key features of Vyne, along with runnable demos.

This guide itself is powered by Vyne:

 * The Vyne dashboard and developer tools for this demo is available at [https://vyne.guide.vyne.co](https://vyne.guide.vyne.co)
 * The full source of the guides are available [here](https://gitlab.com/vyne/playground)
 * The backend services are running the code from our Rewards Points demo project, available [here](https://gitlab.com/vyne/demos/tree/master/rewards)
 
## Using this guide
This guide demonstrates some of the key features of Vyne, along with runnable demos.

Each of the runnable demos show the schema of the services and types that are used within the demos.  These are described in [Taxi](https://taxilang.org), 
which is Vyne's native format for storing schemas. 

By clicking on the run button, you can see the query executed by Vyne, and the results.  You can also see all of the requests made by Vyne, and click to explore their parameters and responses. 
 
> ## Look ma, no plumbing!
> Vyne automates away the plumbing between services.
>  
> In the examples that follow, there is no hand-written integration code to fetch or submit data between services - Vyne takes care of all of it.
>
> Vyne queries typically take the form of `given(someThing).discover(someOtherThing)`.  
>
> As we'll see, it doesn't matter if `someOtherThing` is fetched from a single service, or if it takes multiple hops to retreive it.  So long as there's a path, Vyne will find a way. 
  
