## Calling your first service

This is a simple "Hello World" example, showing a single type and a service.

We have a `Customer` type with a few attributes, and a service that exists to look up customers by their email address.

Let's take a look, and see how Vyne works:

[[demo]]
