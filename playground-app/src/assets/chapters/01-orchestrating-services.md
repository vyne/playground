## Calling services

Let's expand our models a little, and introduce a new concept - rewards cards.

Customers have rewards cards, but their details are kept in a separate service run by our marketing department.

Let's try and look up the rewards card number for our customer, still using his email address.

[[demo]]

A couple of interesting points here:

 * There was no service to retrieve rewards cards by email address.
 * So, Vyne looked up the customer record, by email, and fetched the `customerId` from the record, in order to pass it over to the `MarketingService`.
 * The `MarketingService` returned  `CustomerMarketingRecord`, which wasn't really what we were after either, so Vyne plucked the `RewardsCardNumber` we were after from there, and returned it to us.

All this, and the total amount of code didn't really change.


