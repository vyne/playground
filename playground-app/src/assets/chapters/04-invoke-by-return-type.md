## Discover services by response
Vyne can discover and invoke services by their return type - both the direct type returned, or a type somewhere in it's hierarchy.  
As we'll see, the services themselves can have very different inputs, and Vyne will construct request objects (discovering data for inputs where neccessary), and then call them.

For this example, we'll create a type of `Promotion`, for showing promotions to customers:

```kotlin
type Promotion {
  availableUntil : Date
  title : String
}
```

Then, define a couple of promotion return types:

```
type DiscountPromotion inherits Promotion {
  message : String
}


type DoublePointsPromotion inherits Promotion {
  message : String
  shops : String[]
}
```

Note that in addition to the properties inherited from `Promotion`, they also expose a couple of other properties unique to their types.

This time, rather we'll ask Vyne to `gather<Promotion>`, rather than `discover`.  This tells Vyne to find all the services returning a `Promotion`, construct a request, and invoke the service.

Let's take a look:

[[demo]]

Note that the request objects were very different - Vyne had to make several calls to fetch the inputs required to construct the objects.

The result is really powerful - we received:

 * Responses from services we know nothing about...
 * ...with request objects for which we had none of the parameters
 
This example also adapts with you.  To add a new promotion, or remove one no longer required, just add or remove an endpoint respectively, no plubming required.

