import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CallExplorerComponent } from './call-explorer.component';

describe('CallExplorerComponent', () => {
  let component: CallExplorerComponent;
  let fixture: ComponentFixture<CallExplorerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CallExplorerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CallExplorerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
