import {Component, Input} from '@angular/core';
import {Chapter} from "../guide/chapter.component";
import * as _ from "lodash";

@Component({
  selector: 'app-table-of-contents',
  templateUrl: './table-of-contents.component.html',
  styleUrls: ['./table-of-contents.component.scss']
})
export class TableOfContentsComponent {
  private _chapters: Chapter[];

  @Input()
  get chapters(): Chapter[] {
    return this._chapters;
  }

  set chapters(value: Chapter[]) {
    this._chapters = value;
    this.createLinks();
  }

  links: Link[];


  private createLinks() {
    this.links = this.chapters.map(chapter => {
      const title = _.upperFirst(_.words(chapter.slug).join(" "));
      return new Link(chapter.slug, title)
    })
  }
}


export class Link {
  constructor(readonly slug: string, readonly title: string) {
  }

  get href():string {
    return "#" + this.slug;
  }
}
