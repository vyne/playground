import {Component} from '@angular/core';
import {Fact, QueryMode} from "./taxi-viewer/query.service";
import {Chapter, Demo} from "./guide/chapter.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  chapters: Chapter[] = [
    new Chapter("assets/chapters/introduction.md", "introduction"),
    new Chapter("assets/chapters/00-simple-example.md",
      "simple-example",
      new Demo(
        ['demo.Customer', 'io.vyne.demos.rewards.CustomerService@@getCustomerByEmail'],
        [new Fact("demo.CustomerEmailAddress", "jimmy@demo.com")],
        "demo.Customer")
    ),
    new Chapter("assets/chapters/01-orchestrating-services.md",
      "orchestrating-services",
      new Demo(
        ['demo.Customer', 'io.vyne.demos.rewards.CustomerService@@getCustomerByEmail', 'io.vyne.demos.marketing.MarketingService@@getMarketingDetailsForCustomer'],
        [new Fact("demo.CustomerEmailAddress", "jimmy@demo.com")],
        "demo.RewardsCardNumber")
    ),
    new Chapter("assets/chapters/02-multiple-hops.md",
      "orchestrating-more-services",
      new Demo(
        [
          'demo.Customer',
          'io.vyne.demos.rewards.CustomerService@@getCustomerByEmail',
          'io.vyne.demos.marketing.MarketingService@@getMarketingDetailsForCustomer',
          'io.vyne.demos.rewards.balances.RewardsBalanceService@@getRewardsBalance'
        ],
        [new Fact("demo.CustomerEmailAddress", "jimmy@demo.com")],
        "demo.RewardsAccountBalance")
    ),
    new Chapter("assets/chapters/03-constraints.md",
      "exploring-constraints",
      new Demo(
        [
          'demo.Customer',
          "io.vyne.demos.marketing.shop.RewardsShopService@@getAvailableRewards",
          "io.vyne.demos.rewards.balances.RewardsBalanceService@@convert"
        ],
        [new Fact("demo.CustomerEmailAddress", "jimmy@demo.com")],
        "io.vyne.demos.marketing.shop.AvailableRewards"
      )
    ),
    new Chapter("assets/chapters/04-invoke-by-return-type.md",
      "function-discovery",
      new Demo(
        [
          "demo.CustomerEmailAddress",
          "io.vyne.demos.marketing.promotions.DiscountPromotionService@@getDiscountPromotion",
          "io.vyne.demos.marketing.promotions.DiscountPromotionService@@getDoublePointsPromotion"
        ],
        [new Fact("demo.CustomerEmailAddress", "jimmy@demo.com")],
        "demos.Promotion",
        QueryMode.GATHER
      )
    )
  ];


  headerLinks: Link[] = [
    {title: "Docs", href: "https://docs.vyne.co"},
    {title: "Issues", href: "https://gitlab.com/vyne/vyne/issues"},
    {title: "Forums", href: "https://spectrum.chat/vyne"}
  ];
  screenWidth: number;

  constructor() {
    // set screenWidth on page load
    this.screenWidth = window.innerWidth;
    window.onresize = () => {
      // set screenWidth on screen size change
      this.screenWidth = window.innerWidth;
    };
  }s
}

export interface Link {
  title: string,
  href: string
}
