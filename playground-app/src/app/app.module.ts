import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Provider} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import {MatButtonModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule} from '@angular/material';
import {TaxiViewerModule} from "./taxi-viewer/taxi-viewer.module";
import {GuideComponent} from './guide/guide.component';
import {ChapterComponent} from './guide/chapter.component';
import {ShowdownModule} from "ngx-showdown";
import { TableOfContentsComponent } from './table-of-contents/table-of-contents.component';
import { InViewportModule, WindowRef } from '@thisissoon/angular-inviewport';
import { ScrollSpyModule } from '@thisissoon/angular-scrollspy';

// Provide window object for browser and a suitable replacement
// on other platforms
const getWindow = () => window;
const providers: Provider[] = [{ provide: WindowRef, useFactory: getWindow }];

@NgModule({
  declarations: [
    AppComponent,
    GuideComponent,
    ChapterComponent,
    TableOfContentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,

    TaxiViewerModule,
    ShowdownModule,

    InViewportModule.forRoot(providers), ScrollSpyModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
