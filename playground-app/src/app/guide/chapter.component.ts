import {Component, Input} from '@angular/core';
import {Fact, QueryMode} from "../taxi-viewer/query.service";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-chapter',
  styleUrls: ['./chapter.component.scss'],
  template: `
    <showdown [value]="prelude" tables="true"></showdown>
    <div class="demo-container" *ngIf="chapter?.demo">
      <app-taxi-viewer [membersToInclude]="chapter.demo.membersToInclude"
                       [facts]="chapter.demo.facts"
                       [targetType]="chapter.demo.targetType"
                       [queryMode]="chapter.demo.queryMode">
      </app-taxi-viewer>
    </div>
    <showdown *ngIf="epilogue" [value]="epilogue" tables="true"></showdown>
  `
})
export class ChapterComponent {
  private _chapter: Chapter;
  @Input()
  get chapter(): Chapter {
    return this._chapter;
  }

  set chapter(value: Chapter) {
    this._chapter = value;
    this.loadContent();
  }

  constructor(private httpClient: HttpClient) {
  }

  markdown: string;
  prelude: string;
  epilogue: string;

  private loadContent() {
    this.httpClient.get(this.chapter.markdown, {responseType: 'text'})
      .subscribe(result => {
        this.markdown = result;

        let parts = this.markdown.split("[[demo]]");
        this.prelude = parts[0];
        if (parts.length > 1) this.epilogue = parts[1];
      })
  }
}


export class Chapter {
  constructor(readonly markdown: string, readonly slug: string, readonly demo?: Demo) {
  }
}

export class Demo {
  constructor(readonly  membersToInclude: string[], readonly facts: Fact[], readonly targetType: string, readonly queryMode: QueryMode = QueryMode.DISCOVER) {
  }
}
