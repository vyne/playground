import {Component, Input} from '@angular/core';
import {Chapter} from "./chapter.component";

@Component({
  selector: 'app-guide',
  styleUrls: ['./guide.component.scss'],
  template: `
    <div class="container">
      <div class="scrollspy-container" *ngFor="let chapter of chapters">
        <div class="scrollspy-container-inner">
          <sn-scroll-spy-section for="toc" [id]="chapter.slug" [attr.id]="chapter.slug">
            <div class="chapter-container">
              <app-chapter [chapter]="chapter"></app-chapter>
            </div>
          </sn-scroll-spy-section>
        </div>
       
      </div>
    </div>
  `
})
export class GuideComponent {

  @Input()
  chapters: Chapter[];
}
